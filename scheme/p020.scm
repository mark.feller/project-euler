#!/usr/local/bin/guile \
-e main -s
!#

(use-modules (srfi srfi-1))

(define (fact n)
  (if (zero? n) 1
      (* n (fact (- n 1)))))

(define (break n)
  (if (zero? n) '()
      (cons (modulo n 10) (break (floor (/ n 10))))))

(define (main args)
  (display (fold + 0 (break (fact 100))))
  (newline))
