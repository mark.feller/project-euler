#!/usr/local/bin/guile \
-e main -s
!#

(use-modules (srfi srfi-1))

(define divisors '(11 12 13 14 15 16 17 18 19 20))

(define (divis? n)
  (fold (lambda (a b) (and a b)) #t (map (lambda (x) (zero? (modulo n x))) divisors)))

(define (find-num n)
  (if (divis? n) n
      (find-num (+ n 20))))

(define (main args)
  (display (find-num 2520))
  (newline))
