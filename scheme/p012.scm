#!/usr/local/bin/guile \
-e main -s
!#

;;; Code:

(use-modules (math primes)
	     (srfi srfi-1))

(define (dedupe e)
  (if (null? e) '()
      (cons (car e) (dedupe (filter (lambda (x) (not (equal? x (car e))))
				    (cdr e))))))

(define (num-divisors n)
  (fold (lambda (a b) (* (+ a 1) b))
	1
	(map (lambda (y) (length (filter (lambda (x) (eq? y x)) (factor n))))
	     (dedupe (factor n)))))

(define (find-triangle-size> n x count)
  (if (> (num-divisors x) n) x
      (find-triangle-size> n (+ x count) (+ count 1))))

(define (main args)
  (display (find-triangle-size> 500 0 1))
  (newline))

;;; End:
