#!/usr/local/bin/guile \
-e main -s
!#

(use-modules (srfi srfi-1))

(define (pow base exp)
  (if (zero? exp) 1
      (* base (pow base (- exp 1)))))

(define (break n)
  (if (zero? n) '()
      (cons (modulo n 10) (break (floor (/ n 10))))))

(define (main args)
  (display (fold + 0 (break (pow 2 1000))))
  (newline))
