#!/usr/local/bin/guile \
-e main -s
!#

;;; Problem 1: Multiples of 3 and 5
;;
;; If we list all the natural numbers below 10 that are multiples of 3
;; or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
;;
;; Find the sum of all the multiples of 3 or 5 below 1000.

;;; Code:

(define (multiple? n)
  (if (or (zero? (modulo n 5)) (zero? (modulo n 3))) n
      0))

(define (sum n)
  (if (zero? n) 0
      (+ (multiple? n) (sum (- n 1)))))

(define (main args)
  (display (sum (- 1000 1)))
  (newline))

;; End:
