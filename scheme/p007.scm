#!/usr/local/bin/guile \
-e main -s
!#

;;; Problem 7:

;; By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we
;; can see that the 6th prime is 13.

;; What is the 10 001st prime number?

;;; Code:

(define (factor? n p)
  (if (null? p) #t
      (if (zero? (modulo n (car p))) #f
          (factor? n (cdr p)))))

(define (nth-prime goal)
  (let ((n 2)
        (p '()))
    (prime-sieve-nth n p goal)))

(define (next-prime n p)
  (if (factor? n p) n
      (next-prime (+ n 1) p)))

(define (prime-sieve-nth n p goal)
  (if (= goal (length p)) (car p)
      (let ((next (next-prime n p)))
        (prime-sieve-nth next (cons next p) goal))))

(define (main args)
  (display (nth-prime 10001))
  (newline))

;; End:
