#!/usr/local/bin/guile \
-e main -s
!#

;;; Problem 10: Summation of primes

;; The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

;; Find the sum of all the primes below two million.

;;; Code:

(use-modules (srfi srfi-1)
             (math primes))

(define (primes< n)
  (make-primes< n '(2)))

(define (make-primes< n p)
  (let ((next (prime> (car p))))
    (if (> next n) p
        (make-primes< n (cons next p)))))

(define (main args)
  (display (fold + 0 (primes< 2000000)))
  (newline))

;; End:
