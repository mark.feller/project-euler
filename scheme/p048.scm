#!/usr/local/bin/guile \
-e main -s
!#

;;; Problem 48: Self powers

;; The series, 11 + 22 + 33 + ... + 1010 = 10405071317.

;; Find the last ten digits of the series, 11 + 22 + 33 + ... + 10001000.

;;; Code:

(use-modules (srfi srfi-1))

(define (enumerate-interval low high)
  (if (> low high) '()
      (cons low (enumerate-interval (+ low 1) high))))

(define (calc-pow e-prime c base e mod)
  (if (eq? e e-prime) c
      (calc-pow (+ e-prime 1) (modulo (* c base) mod) base  e mod)))

(define (modular-pow base exp mod)
  (if (= mod 1) 0
      (calc-pow 0 1 base exp mod)))

(define (main args)
  (let ((mod 10000000000)
        (interval 1000))
    (display (modulo (fold + 0 (map (lambda (x) (modular-pow x x mod))
                                    (enumerate-interval 1 interval)))
                     mod))
    (newline)))

;; End:
