#!/usr/local/bin/guile \
-e main -s
!#

(use-modules (srfi srfi-1))

(define (enumerate-interval low high)
  (if (> low high) '()
      (cons low (enumerate-interval (+ low 1) high))))

(let ((nums (enumerate-interval 1 100)))
  (- (let ((sum (fold + 0 nums)))
       (* sum sum))
     (fold + 0 (map (lambda (x) (* x x)) nums))))
