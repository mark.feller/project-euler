(define-module (math my-primes)
  #:export (prime?))

(define (sieve-check n)
  (let ((r (floor (sqrt n)))
        (f 5))
    (while (<= f r)
      (if (zero? (modulo n f)) #f
          (if (zero? (modulo n (+ f 2))) #f
              (set! f (+ f 6)))))
    #t))

(define (prime? n)
  (cond ((= n 1) #t)
        ((< n 4) #t)
        ((even? n) #f)
        ((< n 9) #t)
        ((zero? (modulo n 3)) #f)
        (else (sieve-check n))))
