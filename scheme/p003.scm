#!/usr/local/bin/guile \
-e main -s
!#

;;; Problem 3: Largest prime factor

;; The prime factors of 13195 are 5, 7, 13 and 29.

;; What is the largest prime factor of the number 600851475143 ?

;;; Code:

(use-modules (math primes))

(define (fact n)
  (if (prime? n) (list n)
      (let* ((p (smallest-prime-divisor n 2))
             (m (/ n p)))
        (cons p (fact m)))))

(define (smallest-prime-divisor n p)
  (if (zero? (modulo n p)) p
      (smallest-prime-divisor n (prime> p))))

(define (main args)
  (let ((factor (car (reverse (fact 600851475143)))))
    (display factor)
    (newline)))

;; End:
